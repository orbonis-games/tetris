import { Point } from "pixi.js";
import _ from "lodash";

interface ShapeData {
    colour: number;
    layouts: number[][][];
}

const ShapeDefinitions: { [key: string]: ShapeData } = {
    "L": {
        colour: 0xcf4444,
        layouts: [
            [
                [1, 0, 0],
                [1, 1, 1]
            ],
            [
                [1, 1],
                [1, 0],
                [1, 0]
            ],
            [
                [1, 1, 1],
                [0, 0, 1]
            ],
            [
                [0, 1],
                [0, 1],
                [1, 1]
            ]
        ]
    },
    "R": {
        colour: 0xcf9e44,
        layouts: [
            [
                [1, 1, 1],
                [1, 0, 0]
            ],
            [
                [1, 1],
                [0, 1],
                [0, 1]
            ],
            [
                [0, 0, 1],
                [1, 1, 1]
            ],
            [
                [1, 0],
                [1, 0],
                [1, 1]
            ]
        ]
    },
    "I": {
        colour: 0x9044cf,
        layouts: [
            [
                [1, 1, 1, 1]
            ],
            [
                [1],
                [1],
                [1],
                [1]
            ]
        ]
    },
    "O": {
        colour: 0xf2f22c,
        layouts: [
            [
                [1, 1],
                [1, 1]
            ]
        ]
    },
    "T": {
        colour: 0x3ac770,
        layouts: [
            [
                [1, 0],
                [1, 1],
                [1, 0]
            ],
            [
                [1, 1, 1],
                [0, 1, 0]
            ],
            [
                [0, 1],
                [1, 1],
                [0, 1]
            ],
            [
                [0, 1, 0],
                [1, 1, 1]
            ]
        ]
    },
    "S": {
        colour: 0x3aabc7,
        layouts: [
            [
                [0, 1],
                [1, 1],
                [1, 0]
            ],
            [
                [1, 1, 0],
                [0, 1, 1]
            ],
            [
                [0, 1],
                [1, 1],
                [1, 0]
            ],
            [
                [1, 1, 0],
                [0, 1, 1]
            ]
        ]
    },
    "Z": {
        colour: 0xc73ab9,
        layouts: [
            [
                [1, 0],
                [1, 1],
                [0, 1]
            ],
            [
                [0, 1, 1],
                [1, 1, 0]
            ],
            [
                [1, 0],
                [1, 1],
                [0, 1]
            ],
            [
                [0, 1, 1],
                [1, 1, 0]
            ]
        ]
    }
}

export enum ShapeTypes {
    L = "L",
    I = "I",
    O = "O",
    T = "T",
    S = "T",
    Z = "Z"
}

export class Shape {
    public position: Point;
    
    private layouts: number[][][];
    private colour: number;
    private rotation: number;

    constructor(layouts: number[][][], colour: number) {
        this.layouts = layouts;
        this.colour = colour;
        this.rotation = 0;
        this.position = new Point(0, 0);
    }

    public rotate(): void {
        this.rotation++;
        if (this.rotation === this.layouts.length) {
            this.rotation = 0;
        }
    }

    public getLayout(): number[][] {
        return this.layouts[this.rotation];
    }

    public getColour(): number {
        return this.colour;
    }
}

export const GetShape = (shape: ShapeTypes): Shape => {
    const definition = ShapeDefinitions[shape];
    return new Shape(definition.layouts, definition.colour);
}

export const GetRandomShape = (): Shape => {
    const keys = Object.keys(ShapeDefinitions);
    const definition = ShapeDefinitions[keys[_.random(keys.length - 1, false)]];
    return new Shape(definition.layouts, definition.colour);
}
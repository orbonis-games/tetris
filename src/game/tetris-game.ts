import { Board } from "./board";
import { Instructions } from "./instructions";
import { GetRandomShape } from "./shape";
import { Game } from "libs/core-game/src/game/game";
import { GameOver } from "libs/core-game/src/game/renderables/game-over";
import { ScoreBoard } from "libs/core-game/src/game/renderables/score-board";
import { Music } from "libs/core-game/src/game/renderables/music";

export class TetrisGame extends Game {
    private board?: Board;
    private gameOver?: GameOver;
    private scoreBoard?: ScoreBoard;
    private instructions?: Instructions;
    private music?: Music;

    protected async initRenderables(): Promise<void> {
        if (this.app) {
            this.board = new Board("board", this.app);
            this.gameOver = new GameOver("game-over", this.app, { family: "Indie Flower", color: 0xFFFFFF });
            this.scoreBoard = new ScoreBoard("score-board", this.app, { family: "Indie Flower", color: 0xFFFFFF });
            this.instructions = new Instructions("instructions", this.app);
            this.music = new Music("music", this.app);
    
            await super.initRenderables(
                this.board,
                this.scoreBoard,
                this.gameOver,
                this.instructions,
                this.music
            );
    
            this.gameOver.show("Tetris", "start", () => this.setupGame());
        }
    }

    private setupGame(): void {
        if (this.board) {
            this.instructions?.setVisible(false);
            this.scoreBoard?.resetScore();
            this.music?.playMusic();
            this.board.addShape(GetRandomShape());
            this.board.onCommitShape = () => {
                window.setTimeout(() => {
                    const success = this.board?.addShape(GetRandomShape());
                    if (!success) {
                        this.completeGame();
                    }
                });
            };
            this.board.onLineClear = (count: number) => {
                this.scoreBoard?.increaseScore((count >= 4) ? 800 : count * 100);
            }
        }
    }

    private completeGame(): void {
        if (this.board && this.gameOver) {
            this.scoreBoard?.hide();
            this.instructions?.setVisible(true);
            this.board.solidifyGrid();
            this.gameOver.show("Game Over", "try again", () => {
                this.instructions?.setVisible(false);
                this.board?.reset();
                this.board?.addShape(GetRandomShape());
                this.scoreBoard?.resetScore();
            }, this.scoreBoard?.getScore());
        }
    }
}
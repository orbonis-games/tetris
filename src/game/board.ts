import { Container, Graphics, Point, Sprite, Texture } from "pixi.js";
import { Shape } from "./shape";
import _ from "lodash";
import { AdjustmentFilter } from "pixi-filters";
import { Tween } from "@tweenjs/tween.js";
import { Renderable } from "libs/core-game/src/game/renderable";
import { Hex2RGB, RGBColour } from "libs/core-game/src/utils/colour-utils";
import { CollidableShape } from "libs/core-game/src/game/collision";

interface UpdateGridResult {
    grid: number[][];
    collision: boolean;
}

export class Board extends Renderable {
    private readonly gridSize: Point = new Point(15, 20);
    private readonly size: Point = new Point(600, 800);
    private readonly defaultColour: number = 0x222222;
    private readonly solidifyColour: number = 0xCCCCFF;
    private readonly solidifyBackgroundColour: number = 0x222233;
    private readonly defaultDropInterval: number = 1;
    private readonly quickDropInterval: number = 0.1;

    public onCommitShape?: () => void;
    public onLineClear?: (count: number) => void;

    private staticGrid?: number[][];
    private grid?: number[][];
    private container?: Container;
    private graphics?: Graphics;
    private spriteGrid?: Sprite[][];

    private activeShape?: Shape;
    
    private dirty: boolean = true;

    private time: number = -1;
    private paused: boolean = true;
    private dropInterval: number = 1;
    
    public init(): Promise<void> {
        return new Promise((resolve) => {
            window.addEventListener("keydown", (e) => {
                if (this.activeShape) {
                    switch (e.key) {
                        case "ArrowUp":
                            this.activeShape.rotate();
                            this.modifyShapePosition(0, 0);
                            e.preventDefault();
                            break;
                        case "ArrowLeft":
                            this.modifyShapePosition(-1, 0);
                            e.preventDefault();
                            break;
                        case "ArrowRight":
                            this.modifyShapePosition(1, 0);
                            e.preventDefault();
                            break;
                        case "ArrowDown":
                            this.dropInterval = this.quickDropInterval;
                            e.preventDefault();
                            break;
                    }
                    this.dirty = true;
                }
            });

            window.addEventListener("keyup", (e) => {
                if (e.key === "ArrowDown") {
                    this.dropInterval = this.defaultDropInterval;
                }
            });

            this.container = new Container();
            this.container.x = 200;
            this.container.y = 150;
            this.app.stage.addChild(this.container);
    
            this.graphics = new Graphics();
            this.container.addChild(this.graphics);
    
            this.app.loader.reset();
            this.app.loader.add("block", "./assets/block.png");
            this.app.loader.load(() => {
                const texture: Texture = this.app.loader.resources["block"].texture;
                const width: number = this.size.x / this.gridSize.x;
                const height: number = this.size.y / this.gridSize.y;
                this.spriteGrid = [];

                for (let x = 0; x < this.gridSize.x; x++) {
                    const sprites: Sprite[] = [];
                    for (let y = 0; y < this.gridSize.y; y++) {
                        const sprite: Sprite = new Sprite(texture);
                        sprite.x = width * x;
                        sprite.y = height * y;
                        sprite.width = width;
                        sprite.height = height;
                        sprite.filters = [ new AdjustmentFilter() ];
                        this.container?.addChild(sprite);
                        sprites.push(sprite);
                    }
                    this.spriteGrid.push(sprites);
                }

                this.reset();
                resolve();
            });    
        });
    }

    public reset(): void {
        this.staticGrid = Array(this.gridSize.x).fill(0).map(x => Array(this.gridSize.y).fill(this.defaultColour));
        this.grid = _.cloneDeep(this.staticGrid);
        this.dirty = true;
        this.activeShape = undefined;
        this.paused = true;
        this.dropInterval = this.defaultDropInterval;
    }

    public addShape(shape: Shape): boolean {
        if (this.staticGrid && !this.activeShape) {
            this.activeShape = shape;
            this.activeShape.position.x = Math.floor(this.gridSize.x / 2) - (Math.floor(this.activeShape.getLayout().length / 2));
            this.modifyShapePosition(0, 0, true);

            if (this.activeShape) {
                this.time = 0;
                this.paused = false;
            }

            return this.activeShape !== undefined;
        } else {
            return false;
        }
    }

    public solidifyGrid(): void {
        if (this.grid && this.spriteGrid) {
            for (let x = 0; x < this.grid.length; x++) {
                for (let y = 0; y < this.grid[x].length; y++) {
                    if (this.grid[x][y] !== this.defaultColour) {
                        this.grid[x][y] = this.solidifyColour;
                    } else {
                        this.grid[x][y] = this.solidifyBackgroundColour;
                    }
                }
            }
            this.dirty = true;
        }
    }

    public render(delta: number): void {
        if (this.graphics && this.grid && this.spriteGrid && this.dirty) {
            this.graphics.clear();

            this.graphics.lineStyle(1, 0x111111);
            this.graphics.drawRect(0, 0, this.size.x, this.size.y);
            for (let x = 0; x < this.grid.length; x++) {
                for (let y = 0; y < this.grid[x].length; y++) {
                    const filter: AdjustmentFilter = this.spriteGrid[x][y].filters[0] as AdjustmentFilter;
                    const colour: RGBColour = Hex2RGB(this.grid[x][y]) as RGBColour;
                    filter.red = colour.r / 255;
                    filter.green = colour.g / 255;
                    filter.blue = colour.b / 255;

                    this.spriteGrid[x][y].alpha = (this.grid[x][y] !== this.defaultColour) ? 1 : 0.2;
                }
            }
            this.dirty = false;
        }

        if (this.activeShape && !this.paused) {
            this.time += delta;

            if (this.time >= this.dropInterval) {
                this.modifyShapePosition(0, 1);
                this.time = 0;
            }
        }
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    private updateGridWithShape(shape: Shape, grid: number[][]): UpdateGridResult {
        const originalGrid: number[][] = grid;
        grid = _.cloneDeep(grid);
        for (let x = 0; x < shape.getLayout().length; x++) {
            for (let y = 0; y < shape.getLayout()[x].length; y++) {
                const value: number = shape.getLayout()[x][y] * shape.getColour();
                if (value > 0) {
                    if (grid[x + shape.position.x][y + shape.position.y] === this.defaultColour) {
                        grid[x + shape.position.x][y + shape.position.y] = value;
                    } else {
                        return { grid: originalGrid, collision: true };
                    }
                }
            }
        }

        return { grid, collision: false };
    }

    private modifyShapePosition(x: number, y: number, initialDrop: boolean = false): void {
        if (this.staticGrid && this.activeShape) {
            const originalPosition: Point = new Point(this.activeShape.position.x, this.activeShape.position.y);
            this.activeShape.position.x = Math.max(
                0,
                this.activeShape.position.x = Math.min(
                    this.gridSize.x - this.activeShape.getLayout().length,
                    this.activeShape.position.x + x
                )
            );
            this.activeShape.position.y = Math.max(
                0,
                this.activeShape.position.y + y
            );
            const result = this.updateGridWithShape(this.activeShape, this.staticGrid);
            if (result.collision) {
                if (initialDrop) {
                    this.activeShape = undefined;
                } else {
                    const targetY = this.activeShape.position.y;
    
                    this.activeShape.position.x = originalPosition.x;
                    this.activeShape.position.y = originalPosition.y;
                    
                    if (targetY !== originalPosition.y) {
                        this.commitActiveShapePosition();
                    }
                }
            } else {
                this.grid = result.grid;
            }
            this.dirty = true;
        }
    }

    private commitActiveShapePosition(): void {
        if (this.activeShape && this.staticGrid) {
            const staticResult = this.updateGridWithShape(this.activeShape, this.staticGrid);
            this.staticGrid = staticResult.grid;
            this.grid = _.cloneDeep(this.staticGrid);
            this.paused = true;

            this.activeShape = undefined;

            this.dirty = true;
            
            this.clearLines().then(() => {
                if (this.onCommitShape) {
                    this.onCommitShape();
                }
            });
        }
    }

    private clearLines(): Promise<void> {
        return new Promise((resolve) => {
            if (this.staticGrid) {
                const flipGrid = <T>(grid: T[][]): T[][] => {
                    return grid[0].map((_, colIndex) => {
                        return grid.map(row => row[colIndex])
                    });
                };

                const checkForLine = (): number => {
                    const grid = flipGrid(this.staticGrid!);
                    for (let y = 0; y < grid.length; y++) {
                        let line = true;
                        for (let x = 0; x < grid[y].length; x++) {
                            if (grid[y][x] === this.defaultColour) {
                                line = false;
                                break;
                            }
                        }
                        if (line) {
                            grid.splice(y, 1);
                            grid.unshift(Array(this.gridSize.x).fill(this.defaultColour));
                            this.staticGrid = flipGrid(grid);
                            return y;
                        }
                    }
    
                    return -1;
                }
    
                let lines: number[] = [];
                let lineComplete: number = checkForLine();
                while (lineComplete > -1) {
                    lines.push(lineComplete);
                    lineComplete = checkForLine();
                }
    
                if (lines.length > 0) {
                    this.paused = true;

                    for (let y = 0; y < lines.length; y++) {
                        for (let x = 0; x < this.gridSize.x; x++) {
                            this.animateBlock(this.spriteGrid![x][lines[y]]);
                        }
                    }

                    window.setTimeout(() => {
                        const grid = flipGrid(this.staticGrid!);
                        for (let y = 0; y < lines.length; y++) {
                            grid.splice(y, 1);
                            grid.unshift(Array(this.gridSize.x).fill(this.defaultColour));
                        }
                        this.staticGrid = flipGrid(grid);

                        this.dirty = true;

                        if (this.onLineClear) {
                            this.onLineClear(lines.length);
                        }
                        resolve();
                    }, 1200);
                } else {
                    resolve();
                }
            }
        });
    }

    private async animateBlock(block: Sprite): Promise<void> {
        return new Promise((resolve) => {
            new Tween(block)
                .to({ alpha: 0 }, 200)
                .yoyo(true)
                .repeat(4)
                .onComplete(() => {
                    resolve();
                })
                .start();
        });
    }
}
